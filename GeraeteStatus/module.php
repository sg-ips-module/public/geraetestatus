<?php
//by SimonS (fork from Traxanos!: https://github.com/traxanos/SymconTRX/tree/master/MachineState)

class GeraeteStatus extends IPSModule
{

    public function Create()
    {
        //Create Profile
        if (!IPS_VariableProfileExists('GS.State')) {
            IPS_CreateVariableProfile('GS.State', 1);
            IPS_SetVariableProfileAssociation('GS.State', 0, $this->Translate('Off'), 'Power', -1);
            IPS_SetVariableProfileAssociation('GS.State', 1, $this->Translate('On'), 'Power', 0x000000);
            IPS_SetVariableProfileAssociation('GS.State', 2, $this->Translate('Machine running..'), 'Power', 0xFF0000);
            IPS_SetVariableProfileAssociation('GS.State', 3, $this->Translate('Done'), 'Power', 0xFFFF00);
        }

        parent::Create();

        //Create Variables
        if (!$StateID = @$this->GetIDForIdent('STATE')) {
            $StateID = $this->RegisterVariableInteger('STATE', $this->Translate('Status'), 'GS.State', 1);
            IPS_SetIcon($StateID, 'Graph');
        }
        if (!$EnergyID = @$this->GetIDForIdent('ENERGY')) {
            $this->RegisterVariableFloat('ENERGY', $this->Translate('Consumption'), '', 2);
            IPS_SetIcon($EnergyID, 'Electricity');
        }
        if (!$DurationID = @$this->GetIDForIdent('DURATION')) {
            $DurationID = $this->RegisterVariableInteger('DURATION', $this->Translate('Duration'), '', 3);
            IPS_SetIcon($DurationID, 'Clock');
        }

        //Attribute
        $this->RegisterAttributeFloat('StartEnergy', 0);

        //Propertys
        $this->RegisterPropertyInteger('PowerId', 0);
        $this->RegisterPropertyInteger('EnergyId', 0);
        $this->RegisterPropertyInteger('PauseId', 0);
        $this->RegisterPropertyFloat('PowerOff', 0.5);
        $this->RegisterPropertyInteger('DelayOff', 120);
        $this->RegisterPropertyFloat('PowerDone', 2);
        $this->RegisterPropertyInteger('DelayDone', 60);
        $this->RegisterPropertyBoolean('JumpDone2Run', false);
        $this->RegisterPropertyInteger('DelayRun', 90);
        $this->RegisterPropertyBoolean('Debug', false);
        $this->RegisterPropertyBoolean('DebugEnergy', false);

        //Timer
        $this->RegisterTimer('TimerOff', 0, 'GS_Timers($_IPS[\'TARGET\']);');
        $this->RegisterTimer('TimerRun', 0, 'GS_Timers($_IPS[\'TARGET\']);');
        $this->RegisterTimer('TimerDone', 0, 'GS_Timers($_IPS[\'TARGET\']);');

        //Buffer
        $this->SetBuffer('TimerOff', '0');
        $this->SetBuffer('TimerRun', '0');
        $this->SetBuffer('TimerDone', '0');

        $this->SetState(0);
    }

    public function ApplyChanges()
    {
        parent::ApplyChanges();

        //Register Kernel Messages
        $this->RegisterMessage(0, IPS_KERNELSTARTED);

        //IP-Symcon Kernel bereit?
        if (IPS_GetKernelRunlevel() !== KR_READY) {
            $this->SendDebug(__FUNCTION__, $this->Translate('Kernel is not ready! Kernel Runlevel = ') . IPS_GetKernelRunlevel(), 0);
            return false;
        }

        //Variablen ausgewählt?
        if ($this->CheckVariables() === false) {
            return false;
        } else {
            //Setze Profil der Verbrauchs-Variable (von der Energiemessung-Variable)
            $this->RegisterVariableFloat('ENERGY', $this->Translate('Consumption'), IPS_GetVariable($this->ReadPropertyInteger('EnergyId'))['VariableProfile'], 2);
            IPS_SetIcon($this->GetIDForIdent('ENERGY'), 'Electricity');
        }

        //Check OK
        $this->SetStatus(102);

        //Event aktivieren
        if (!$EventID = @IPS_GetObjectIDByIdent('ON_POWER_CHANGE', $this->InstanceID)) {
            $EventID = IPS_CreateEvent(0);
            IPS_SetParent($EventID, $this->InstanceID);
            IPS_SetIdent($EventID, 'ON_POWER_CHANGE');
            IPS_SetHidden($EventID, true);
            IPS_SetName($EventID, 'On power change');
            IPS_SetPosition($EventID, 999);
        }
        IPS_SetEventTrigger($EventID, 0, $this->ReadPropertyInteger('PowerId'));
        IPS_SetEventScript($EventID, 'GS_Update($_IPS[\'TARGET\']);');
        IPS_SetEventActive($EventID, true);

        //Check Done > Off
        if ($this->ReadPropertyFloat('PowerDone') <= $this->ReadPropertyFloat('PowerOff')) {
            $this->SetStatus(201);
            return false;
        }

        //Starte Update
        $this->DebugHeader(); //only for debug
        $this->Update();

        return true;
    }

    public function GetConfigurationForm()
    {
        $InfoArray = $this->ModulInfo();
        $Name = $InfoArray['Name'];
        $Version = $InfoArray['Version'];
        $Build = $InfoArray['Build'];
        return '{
          "elements":
          [
            {"type": "Label", "label": "' . $Name . ' - v' . $Version . ' (Build: ' . $Build . ') © by SimonS"},
            { "type": "Label", "label": "Module for measuring power and energy consumption with ready message. State sequence: Off > On > Running > Done > Off" },
            {
                "type": "ExpansionPanel",
                "caption": "Sensors",
                "items": [
                    { "type": "RowLayout", "items": [ 
                        { "type": "SelectVariable", "name": "PowerId", "caption": "Power (Watt)", "validVariableTypes": [2] },
                        { "type": "Label", "label": "          " },
                        { "type": "SelectVariable", "name": "EnergyId", "caption": "Energy Counter (Wh or kWh)", "validVariableTypes": [2] }
                    ] }
                ]
            },
            {
                "type": "ExpansionPanel",
                "caption": "Condition: finished",
                "items": [
                    { "type": "Label", "label": ".. applies after falling below the value and expiry of the delay time, exceeding = state: Running(only if the Off state was previously reached)" },
                    { "type": "RowLayout", "items": [ 
                        { "type": "NumberSpinner", "name": "PowerDone", "caption": "Watt", "digits": "1", "minimum": "0.2"},
                        { "type": "Label", "label": "          " },
                        { "type": "NumberSpinner", "name": "DelayDone", "caption": "delay (seconds)", "minimum": "0"}
                    ] },
                    { "type": "Label", "label": "Allow the state sequence to jump from Done -> Running" },
                    { "type": "RowLayout", "items": [ 
                        { "type": "CheckBox", "name": "JumpDone2Run", "caption": "Enabled" },
                        { "type": "Label", "label": "          " },
                        { "type": "NumberSpinner", "name": "DelayRun", "caption": "delay (seconds)", "minimum": "0"}
                    ] }
                ]
            },
            {
                "type": "ExpansionPanel",
                "caption": "Condition: off",
                "items": [
                    { "type": "Label", "label": ".. applies after state done, and falling below the value and expiry of the delay time, exceeding = state: On" },
                    { "type": "RowLayout", "items": [ 
                        { "type": "NumberSpinner", "name": "PowerOff", "caption": "Watt", "digits": "1", "minimum": "0.1"},
                        { "type": "Label", "label": "          " },
                        { "type": "NumberSpinner", "name": "DelayOff", "caption": "delay (seconds)", "minimum": "0"}
                    ] }
                ]
            },
            {
                "type": "ExpansionPanel",
                "caption": "Optional",
                "items": [
                    { "type": "Label", "label": "Variable to pause the state sequence, False = Pause" },
                    { "type": "RowLayout", "items": [ 
                        { "type": "SelectVariable", "name": "PauseId", "caption": "Pause (bool)", "validVariableTypes": [0] }
                    ] }
                ]
            },
            { "type": "Label", "label": "________________________________________________________________________________________________________________________________________" },
            {"type": "RowLayout", "items": [
                { "type": "CheckBox", "name": "Debug", "caption": "Debug" },
                { "type": "CheckBox", "name": "DebugEnergy", "caption": "with energy values" }
             ] }
          ],
          "actions":
          [
              {"type": "RowLayout", "items": [
                  { "type": "Button", "label": "Website", "onClick": "echo \'https://ips.air5.net/\';" },
                  { "type": "Button", "label": "Documentation", "onClick": "echo \'https://gitlab.com/sg-ips-module/public/geraetestatus/-/blob/master/README.md\';" }
              ] }
          ],
          "status":
          [
              {"code": 102, "icon": "active", "caption": "Module is active" },
              {"code": 104, "icon": "inactive", "caption": "Module is inactive - Select the sensors variables first!" },
              {"code": 201, "icon": "error", "caption": "ERROR: The value:Done must be greater than value:Off!" }
          ]
      }';
    }

    public function Update()
    {
        if ($this->CheckVariables() === false) {
            return false;
        }

        $OldState = GetValueInteger($this->GetIDForIdent('STATE'));

        /* When pause, return */
        if ($this->CheckPauseVariableExist()) {
            $PauseValue = $this->ReadPropertyInteger('PauseId');
            if (GetValueBoolean($PauseValue) === false) {
                $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] Pausiert wegen Bool-Variable');
                #Stop running done timer
                if ($this->GetBuffer('TimerDone') !== '0') {
                    $this-> TimerStop_3();
                }
                return false;
            }
        }

        $CurrPower = $this->AktuelleLeistung();
        $PowerOff = $this->ReadPropertyFloat('PowerOff');
        $PowerDown = $this->ReadPropertyFloat('PowerDone');

        /* Aus[0] > An[1] > Läuft[2] > Fertig[3] > Aus[0] */
        if ($CurrPower < $PowerDown && $OldState === 2) { //Status: Läuft > Fertig(Timer)
            $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > Check1');
            $this->EnergieMessung(false, false);
            $this->TimerStart_3();
        } elseif ($CurrPower < $PowerOff && $OldState === 3) { //Status: Fertig > Aus(Timer)
            $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] >Check2');
            $this->TimerStart_0();
        } elseif ($CurrPower < $PowerOff && $OldState === 1) { //Status: An > Aus
            $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > Check3');
            $this->SetState(0);
        } elseif ($CurrPower >= $PowerDown) {
            if ($OldState === 0 || $OldState === 1) { //Status: Läuft (wenn Aus oder An)
                $this->DebugHeader();
                $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > Check4');
                $this->EnergieMessung(true, true);
                $this->SetState(2); }
            if ($OldState === 2) {
                $this->TimerStop_3();
            }
            if ($OldState === 3 && $this->ReadPropertyBoolean('JumpDone2Run')) { //Wenn Springen erlaubt (Fertig > Läuft)
                    $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > Check5');
                    $this->TimerStart_2();
            } else {
                $this->EnergieMessung(false, false);
            }
        } elseif ($CurrPower >= $PowerOff) {
            //Status: Aus > An
            if ($OldState === 0) {
                $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > Check6');
                $this->SetState(1);
            }
        }
        return true;
    }

    public function CheckVariables()
    {
        if (!IPS_VariableExists($this->ReadPropertyInteger('PowerId')) || !IPS_VariableExists($this->ReadPropertyInteger('EnergyId') )) {
            $this->SetStatus(104);
            $this->myDebug(__FUNCTION__ . '()', 'ERROR: Select Variable!');
            return false;
        }
        return true;
    }

    public function CheckPauseVariableExist()
    {
        if (IPS_VariableExists($this->ReadPropertyInteger('PauseId'))) {
            #$this->myDebug(__FUNCTION__ . '()', $this->ReadPropertyInteger('PauseId'));
            return true;
        }
        return false;
    }

    public function AktuelleLeistung(): float
    {
        $PowerID = $this->ReadPropertyInteger('PowerId');
        $this->myDebug(__FUNCTION__ . '()', (float)GetValue($PowerID) . ' Watt');
        return GetValueFloat($PowerID);
    }

    public function AktuelleEnergie(): float
    {
        $EnergyID = $this->ReadPropertyInteger('EnergyId');
        //$this->myDebug(__FUNCTION__ . '()', GetValueFloat($EnergyID));
        return GetValueFloat($EnergyID);
    }

    public function SetState(int $value)
    {
        if ($this->GetBuffer('TimerDone') === '1') {
            $this->TimerStop_3();
        }
        if ($this->GetBuffer('TimerOff') === '1') {
            $this->TimerStop_0();
        }
        if ($this->GetBuffer('TimerRun') === '1') {
            $this->TimerStop_2();
        }

        SetValueInteger($this->GetIDForIdent('STATE'), $value);
        $this->NeuerStatus($value);
    }

    //für Status: Aus
    public function TimerStart_0()
        {
        if ($this->GetBuffer('TimerOff') === '0') {
            $DelayOff = $this->ReadPropertyInteger('DelayOff') * 1000;
            if ($DelayOff > 0) {
                $this->myDebug(__FUNCTION__ . '(Aus)', 'Verzögerung ' . $DelayOff / 1000 . ' Sekunden');
                $this->SetBuffer('TimerOff', '1');
                $this->SetTimerInterval('TimerOff', $DelayOff);
            } else {
                $this->myDebug(__FUNCTION__ . '(Aus)', 'Verzögerung deaktiviert (0)');
                $this->SetState(0);
            }
        }
    }
    //für Status: Aus
    public function TimerStop_0()
    {
        $this->myDebug(__FUNCTION__ . '(Aus)', 'Verzögerung abgelaufen');
        $this->SetBuffer('TimerOff', '0');
        $this->SetTimerInterval('TimerOff', 0);
    }
    //für Status: Läuft
    public function TimerStart_2()
    {
        if ($this->GetBuffer('TimerRun') === '0') {
            $DelayRun = $this->ReadPropertyInteger('DelayRun') * 1000;
            if ($DelayRun > 0) {
                $this->myDebug(__FUNCTION__ . '(Läuft)', 'Verzögerung ' . $DelayRun / 1000 . ' Sekunden');
                $this->SetBuffer('TimerRun', '1');
                $this->SetTimerInterval('TimerRun', $DelayRun);
            } else {
                $this->myDebug(__FUNCTION__ . '(Läuft)', 'Verzögerung deaktiviert (0)');
                $this->SetState(2);
            }
        }
    }
    //für Status: Läuft
    public function TimerStop_2()
    {
        $this->myDebug(__FUNCTION__ . '(Läuft)', 'Verzögerung abgelaufen');
        $this->SetBuffer('TimerRun', '0');
        $this->SetTimerInterval('TimerRun', 0);
    }
    //für Status: Fertig
    public function TimerStart_3()
    {
        if ($this->GetBuffer('TimerDone') === '0') {
            $DelayDone = $this->ReadPropertyInteger('DelayDone') * 1000;
            if ($DelayDone > 0) {
                $this->myDebug(__FUNCTION__ . '(Fertig)', 'Verzögerung ' . $DelayDone / 1000 . ' Sekunden');
                $this->SetBuffer('TimerDone', '1');
                $this->SetTimerInterval('TimerDone', $DelayDone);
            } else {
                $this->myDebug(__FUNCTION__ . '(Fertig)', 'Verzögerung deaktiviert (0)');
                $this->SetState(3);
            }
        }
    }
    //für Status: Fertig
    public function TimerStop_3()
    {
        $this->myDebug(__FUNCTION__ . '(Fertig)', 'Verzögerung abgelaufen, oder Wertüberschreitung');
        $this->SetBuffer('TimerDone', '0');
        $this->SetTimerInterval('TimerDone', 0);
    }

    public function Timers()
    {
        $CurrPower = $this->AktuelleLeistung();
        $PowerOff = $this->ReadPropertyFloat('PowerOff');
        $PowerDown = $this->ReadPropertyFloat('PowerDone');
        $OldState = GetValueInteger($this->GetIDForIdent('STATE'));

        /* Off=0, On=1, Machine running=2, Done=3 */
        if ($CurrPower < $PowerDown && $OldState === 2) { //State: Done
            $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > [3]');
            $this->SetState(3);
        } elseif ($CurrPower < $PowerOff && $OldState === 3) { //State: Off
            $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > [0]');
            $this->SetState(0);
        } elseif ($CurrPower > $PowerDown && $OldState === 3 && $this->ReadPropertyBoolean('JumpDone2Run')) { //State: Run
            $this->myDebug(__FUNCTION__ . '()', '[' . $OldState .'] > [2]');
            $this->SetState(2);
        } else {
            $this->myDebug(__FUNCTION__ . '()', 'Alle Timer beenden');
            $this->TimerStop_0();
            $this->TimerStop_2();
            $this->TimerStop_3();
        }
    }

    private function EnergieMessung($ResetEnergy, $ResetTime)
    {
        if ($ResetTime) {
            if ($this->ReadPropertyBoolean('DebugEnergy')) {
                $this->myDebug(__FUNCTION__ . '()', 'Dauer zurücksetzen');
            }
            $this->SetBuffer('StartTime', time());
            SetValueInteger($this->GetIDForIdent('DURATION'), 0);
        } else {
            $CurDuration = ceil((time() - (int)$this->GetBuffer('StartTime')) / 60);
            SetValueInteger($this->GetIDForIdent('DURATION'), $CurDuration);
            //$this->myDebug(__FUNCTION__ . '()', 'Set duration: ' . $CurrDuration);
        }
        if ($ResetEnergy) {
            if ($this->ReadPropertyBoolean('DebugEnergy')) {
                $this->myDebug(__FUNCTION__ . '()', 'Energie zurücksetzen (' . $this->AktuelleEnergie() . ')');
            }
            $this->WriteAttributeFloat('StartEnergy', $this->AktuelleEnergie());
            SetValueFloat($this->GetIDForIdent('ENERGY'), 0);
        } else {
            $AktuelleEnergie = $this->AktuelleEnergie() - $this->ReadAttributeFloat('StartEnergy');
            SetValueFloat($this->GetIDForIdent('ENERGY'), $AktuelleEnergie);
            if ($this->ReadPropertyBoolean('DebugEnergy')) {
                $this->myDebug(__FUNCTION__ . '()', $AktuelleEnergie . ' Watt'); //wieder einschalten!
            }
        }
    }

    private function NeuerStatus($value)
    {
        /* Aus[0] > An[1] > Läuft[2] > Fertig[3] > Aus[0] */
        switch ($value) {
            case 0:
                $NeuerStatus = '[' . $value . '] Aus';
                break;
            case 1:
                $NeuerStatus = '[' . $value . '] Ein';
                break;
            case 2:
                $NeuerStatus = '[' . $value . '] Läuft';
                $this->DebugHeader();
                break;
            case 3:
                $NeuerStatus = '[' . $value . '] Fertig';
                break;
            default:
                $NeuerStatus = '[' . $value . '] Error!';
        }
        #$this->DebugHeader();
        $this->myDebug(__FUNCTION__ . ' >>>>>>>', $NeuerStatus);
    }

    private function DebugHeader()
    {
        if ($this->ReadPropertyBoolean('Debug')) {
            $InfoArray = $this->ModulInfo();
            $Name = $InfoArray['Name'];
            $Version = $InfoArray['Version'];
            $Build = $InfoArray['Build'];
            $PowerOff = $this->ReadPropertyFloat('PowerOff');
            $PowerDown = $this->ReadPropertyFloat('PowerDone');
            $DelayOff = $this->ReadPropertyInteger('DelayOff');
            $DelayDone = $this->ReadPropertyInteger('DelayDone');
            $DelayRun = $this->ReadPropertyInteger('DelayRun');
            $this->myDebug(__FUNCTION__ . '()', $Name . ' - v' . $Version .' (Build:' . $Build . ')');
            $this->myDebug('Statuswerte:', '0=Aus > 1=An > 2=Läuft > 3=Fertig > 0=Aus');
            $this->myDebug('Einstellungen', 'Fertig: ' . $PowerDown . ' Watt / ' . $DelayDone . ' Sekunden');
            $this->myDebug('Einstellungen', 'Aus: ' . $PowerOff . ' Watt / ' . $DelayOff . ' Sekunden');
            if ($this->ReadPropertyBoolean('JumpDone2Run')) {
                $this->myDebug('Einstellungen', 'Erlaube springen von Fertig[3] > Läuft[2] / ' . $DelayRun . ' Sekunden');
            }
        }
    }

    private function ModulInfo()
    {
        return IPS_GetLibrary('{34D9DAE7-3406-7533-F535-AE6125AA4088}');
    }

    private function myDebug($Message, $DATA)
    {
        if ($this->ReadPropertyBoolean('Debug')) {
            $this->SendDebug($Message, $DATA, 0);
        }
    }
}
