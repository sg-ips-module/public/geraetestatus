# GeraeteStatus

[![Version](https://img.shields.io/badge/Symcon-PHPModul-red.svg)](https://www.symcon.de/)
[![Version](https://img.shields.io/badge/Modul%20Version-1.3%20Build:241005-blue.svg)]()
[![Version](https://img.shields.io/badge/Symcon%20Version->=%205.0-green.svg)]()
[![Version](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-orange.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de)
[![Version](https://img.shields.io/badge/Spenden-PayPal-yellow.svg?style=flat-square)](https://www.paypal.com/paypalme2/Simon6714?locale.x=de_DE)

---

Das Modul ermittelt den Zustand eines Gerätes (Waschmaschine, Wäschetrockner) über den Stromverbrauch.  
Der Zustands-Ablauf ist: Aus(0) > An(1) > Läuft(2) > Fertig(3) > Aus(0).  
Zustand 3 kann für Fertigmeldung genutzt werden.

## 📝 Inhaltsverzeichnis

- [Vorwort](#Vorwort)
- [Funktionsumfang](#Funktionsumfang)
- [Systemanforderungen](#Systemanforderungen)
- [Installation](#Installation)
- [Benachrichtigung](#Benachrichtigung)
- [Autoren](#Autoren)
- [Lizenz](#Lizenz)
- [Screenshots](#Screenshots)
- [FAQ](#FAQ)


## ☞ Vorwort <a name = "Vorwort"></a>
Die Logik wurde mit Version 1.2 (Build: 200307) neu gestaltet, und entspricht nicht mehr dem [Ursprung-Projektes](https://github.com/traxanos/SymconTRX/tree/master/MachineState).  
Zum Beitrag im [Symcon Forum](https://www.symcon.de/forum/threads/43093-Modul-GeraeteStatus-(Fertigmelder)-mit-Energiemessung).

- Kein überspringen der Zustände
- Überschreitung des Wertes unterbricht die Verzögerungszeit(Timer)
- Die eingestellte Verzögerungszeit gilt immer (Wert 0 deaktiviert die Verzögerung)
- Nach Ende der Verzögerungszeit findet erneute Überprüfung des Power/Watt Wertes statt
- Der Energiezähler beginnt bei Zustand Läuft(2) zu zählen, mit Voraussetzung der Status zuvor Aus(0) war
- Der Wechsel von 'Aus(0)' zu 'An(1)' setzt den Zählerstatus nicht zurück
- Debug Mode Ausgabe hilft die richtigen Werte zu finden
- Optional kann der Zustands-Ablauf mit einer Bool Variable pausiert werden (false=pause), siehe FAQ

---

## 🧐 Funktionsumfang <a name = "Funktionsumfang"></a>

Das Modul ermittelt den Zustand eines Gerätes über den Leistungswert einer Messsteckdose mit Leistungs- und Energiemessung.  

- Anzeige des Zustandes (Aus, An, Maschine läuft, Fertig, Aus)
- Anzeige der Dauer (Dauer seit Zustand: Läuft)
- Anzeige des Energieverbrauchs (Verbrauch seit Zustand: Läuft)
- Verzögerungszeit in Sekunden (vermeidet Fertigmeldungen bei Knitter-Schutz des Wäschetrockners)
- Benachrichtigung für Waschmaschine ist fertig (Symcon Action-Script)

Der Zustands-Ablauf ist: Aus(0) > An(1) > Läuft(2) > Fertig(3) > Aus(0).

##### Getestete Hardware:

- HomeMatic Funk-Schaltaktor 1-fach mit Leistungsmessung, Zwischenstecker Typ F (HM-ES-PMSw1-PI)  
- Shelly Plug S

## 🏁 Systemanforderungen <a name = "Systemanforderungen"></a>

- IP-Symcon ab Version 6.0 (keine Legacy Konsole Unterstützung!)
- Energie Mess-Steckdose die den Leistungs- und Verbrauchswert (als Float-Variable) zur Verfügung stellt

## 🔧 Installation <a name = "Installation"></a>

Das Modul in IP-Symcon einbinden:

1. In der IP-Symcon Console unter "Kern Instanzen" die "Module Control" Instanz öffnen
2. Auf "Hinzufügen" klicken, hier ist die Modul GIT-URL einzutragen:
    <https://gitlab.com/SG-IPS-Module/Public/GeraeteStatus.git>
3. Nun kann im IPS-Objektbaum (an beliebiger Stelle) eine neue Modul-Instanz hinzufügt werden

## 🖂 Benachrichtigung <a name = "Benachrichtigung"></a>

Das Modul selbst beinhaltet keine eigene Benachrichtigung-Funktion, dies kann jedoch leicht über ein Script erledigt werden.

#### Beispiel Benachrichtigung Script

```php
<?php
//Benachrichtigung Wäschetrockner

$Timestamp = date('Y.m.d - H:i');
$Anwesend = GetValue(42404);

$EchoWZ = 47420;
$MailAD = 17188;
$Bot = 17399;
$Webfront = 18102;

//Text
$Text = 'Der Wäschetrockner ist fertig.';
$Message = 'Hallo, der Wäschetrockner ist fertig.';

//Webfront
WFC_SendNotification($Webfront, $Timestamp, $Text, "IPS", 3600);  

//Amazon echo
if ( $Anwesend === true ) {
    EchoRemote_TextToSpeech($EchoWZ, $Message);
}  

//Mail
if ( $Anwesend === false ) {
    SMTP_SendMail($MailAD, 'Haussteuerung', $Text);
}  

//Telegram
if ( $Anwesend === false ) {
    Telegram_SendText($Bot, $Text,"987654321");
}
```

Dem Skript muss anschließend nur noch ein "Ereignis->Ausgelöst" hinzufügt werden, Auslöser: Modul-Variable "Zustand" = 3.  

## ✍️ Autoren <a name = "Autoren"></a>

- [@Simon6714](https://github.com/Simon6714) 
- [@Traxanos](https://github.com/traxanos) - Entwickler des Ursprungsmoduls (siehe Vorwort)

## 📄 Lizenz <a name = "Lizenz"></a>

Dieses Projekt unterliegt der [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de) Lizenz, und ist für die nicht kommerzielle Nutzung kostenlos.  

## 👓 Screenshots <a name = "Screenshots"></a>

WebFront:  
![Screenshot1](imgs/GeraeteStatus1.png)

WebFront:  
![Screenshot1](imgs/GeraeteStatus2.png)

Konfigurationsformular:  
![Screenshot2](imgs/GeraeteStatusKF.png)

## 🎉 FAQ <a name = "FAQ"></a>

- Homematic Messsteckdose  
  Bei der Homematic Messsteckdose muss eventuell der Energiekanal (Channel 2) angepasst werden. Standardeinstellung für übermitteln des Leistungswertes ist hier auf 100 Watt voreingestellt.  

- Zustand Aus, wird nie erreicht  
  Wie im Modultext beschrieben wird "Zustand: Aus" nur ausgelöst bei **Unterschreitung** und wenn zuvor "Zustand: Fertig" war.  
  Unterschreitung von Wert "0" bei "Zustand: Aus" kann niemals erreicht werden, wähle einfach einen Wert höher als "0,1 Watt".  
  
- Zustand Aus, Verzögerung ist viel länger  
  Das Modul reagiert nur auf Events des Energiewertes(ausgewählte Watt Instanz).  
  Die Verzögerungszeit kann erst starten(auslösen) wenn vom Energiemesser neue Werte übermittelt werden. 

- Variable Verbrauch  
  Das Modul merkt sich den Start-Wert der Energiemesser-Variable("Zustand: Maschine läuft") und berechnet die Differenz bei jeder Aktualisierung der Leistungs-Variable.  

- Variable Pause  
  Bei Wert=false dieser (bool)Variable wird der Zustandsablauf pausiert (Fertig Timer wird gestoppt).  
  Hilfreich wenn der Ablauf durch eine Schaltsteckdose unterbrochen wird.  
  Siehe [Forenbeitrag](https://community.symcon.de/t/modul-geraetestatus-fertigmelder-mit-energiemessung/52154/38?u=simons)  

- Der Wäschetrockner meldet (Dank Knitter-Schutz) mehrmals hintereinander "Fertig"  
  Das Modul sollte "Zustand: Aus" erst nach dem Ausschalten/entleeren des Wäschetrockners erreichen!  